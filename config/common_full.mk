# Inherit common Mirage stuff
$(call inherit-product, vendor/mirage/config/common.mk)

# Bring in all video files
$(call inherit-product, frameworks/base/data/videos/VideoPackage2.mk)

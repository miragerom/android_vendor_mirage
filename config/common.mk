PRODUCT_BRAND ?= mirage


PRODUCT_COPY_FILES += \
    vendor/mirage/prebuilt/common/bootanimation/bootanimation.zip:system/media/bootanimation.zip


ifdef mirage
PRODUCT_PROPERTY_OVERRIDES += \
    ro.rommanager.developerid=mirage


PRODUCT_BUILD_PROP_OVERRIDES += BUILD_UTC_DATE=0

PRODUCT_PROPERTY_OVERRIDES += \
    keyguard.no_require_sim=true \
    ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
    ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html \
    ro.com.google.clientidbase=android-google \
    ro.com.android.wifi-watchlist=GoogleGuest \
    ro.setupwizard.enterprise_mode=1 \
    ro.com.android.dateformat=MM-dd-yyyy \
    ro.com.android.dataroaming=false

# Copy over the changelog to the device
PRODUCT_COPY_FILES += \
    vendor/mirage/CHANGELOG.mkdn:system/etc/CHANGELOG-mirage.txt

# Backup Tool
PRODUCT_COPY_FILES += \
    vendor/mirage/prebuilt/common/bin/backuptool.sh:system/bin/backuptool.sh \
    vendor/mirage/prebuilt/common/bin/backuptool.functions:system/bin/backuptool.functions \
    vendor/mirage/prebuilt/common/bin/50-mirage.sh:system/addon.d/50-mirage.sh \
    vendor/mirage/prebuilt/common/bin/blacklist:system/addon.d/blacklist

# init.d support
PRODUCT_COPY_FILES += \
    vendor/mirage/prebuilt/common/etc/init.d/00banner:system/etc/init.d/00banner \
    vendor/mirage/prebuilt/common/bin/sysinit:system/bin/sysinit

# userinit support
PRODUCT_COPY_FILES += \
    vendor/mirage/prebuilt/common/etc/init.d/90userinit:system/etc/init.d/90userinit

# mirage-specific init file
PRODUCT_COPY_FILES += \
    vendor/mirage/prebuilt/common/etc/init.local.rc:root/init.mirage.rc

# Compcache/Zram support
PRODUCT_COPY_FILES += \
    vendor/mirage/prebuilt/common/bin/compcache:system/bin/compcache \
    vendor/mirage/prebuilt/common/bin/handle_compcache:system/bin/handle_compcache

# Nam configuration script
PRODUCT_COPY_FILES += \
    vendor/mirage/prebuilt/common/bin/modelid_cfg.sh:system/bin/modelid_cfg.sh

# Enable SIP+VoIP on all targets
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml

# This is mirage!
PRODUCT_COPY_FILES += \
    vendor/mirage/config/permissions/com.cyanogenmod.android.xml:system/etc/permissions/com.cyanogenmod.android.xml


# Required mirage packages
PRODUCT_PACKAGES += \
    Camera \
    Development \
    LatinIME \
    SpareParts \
    Superuser \
    su

PRODUCT_PACKAGE_OVERLAYS += vendor/mirage/overlay/dictionaries
PRODUCT_PACKAGE_OVERLAYS += vendor/mirage/overlay/common

PRODUCT_VERSION_MAJOR = 10
PRODUCT_VERSION_MINOR = 1
PRODUCT_VERSION_MAINTENANCE = 0-RC0

# Set ME_BUILDTYPE

ifdef ME_BUILDTYPE
    ifdef ME_EXTRAVERSION
        # Force build type to RELEASE
        ME_BUILDTYPE := RELEASE
        # Add leading dash to ME_EXTRAVERSION
        ME_EXTRAVERSION := -$(ME_EXTRAVERSION)
    endif

ifdef ME_RELEASE
    ME_VERSION := $(PRODUCT_VERSION_MAJOR).$(PRODUCT_VERSION_MINOR).$(PRODUCT_VERSION_MAINTENANCE)$(PRODUCT_VERSION_DEVICE_SPECIFIC)-$(ME_BUILD)
else
    ifeq ($(PRODUCT_VERSION_MINOR),0)
        ME_VERSION := $(PRODUCT_VERSION_MAJOR)-$(shell date -u +%Y%m%d)-$(ME_BUILDTYPE)-$(mirage_BUILD)$(ME_EXTRAVERSION)
    else
        ME_VERSION := $(PRODUCT_VERSION_MAJOR).$(PRODUCT_VERSION_MINOR)-$(shell date -u +%Y%m%d)-$(ME_BUILDTYPE)-$(mirage_BUILD)$(ME_EXTRAVERSION)
    endif
endif

PRODUCT_PROPERTY_OVERRIDES += \
  ro.mirage.version=$(ME_VERSION) \
  ro.modversion=$(ME_VERSION)


-include $(WORKSPACE)/hudson/image-auto-bits.mk

endif
